import { Component } from '@angular/core';
import {LoadingServiceService} from './shared/services/loading-service.service';
import { NgxSpinnerService } from 'ngx-spinner';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent {
  title = 'my-app';

  constructor(private spinner: NgxSpinnerService, public loadingService : LoadingServiceService) {

  }

  ngOnInit() {

    this.loadingService.status.subscribe( (value) => {

      if(value){

        this.spinner.show();
      } else{

        this.spinner.hide();
      }
    });
  }
}
