import { Component, OnInit } from '@angular/core';
import {AppRequestService} from '../../shared/services/app-request.service';
import {Router} from '@angular/router';
import {ToastrService} from 'ngx-toastr';
import {LoadingServiceService} from '../../shared/services/loading-service.service';
import {SocketService} from '../../shared/services/socket.service';
import swal from 'sweetalert2'

@Component({
  selector: 'app-users',
  templateUrl: './users.component.html',
  styleUrls: ['./users.component.css']
})
export class UsersComponent implements OnInit {

  users : any;
  constructor(public apiRequest : AppRequestService, private toastr: ToastrService, public router: Router,
              public loading : LoadingServiceService, private socketService : SocketService) {

          this.socketService.socket.on('test_Socket' , function (res) {

            console.log(res.test);
          });
  }

  ngOnInit() {

    this.loading.display(true);
    this.apiRequest.getRequest('users').then( (res) => {

      console.log(res);
      this.loading.display(false);
      this.users = res['data'];
    })
    .catch( (err)=> {

      console.log(err);
      this.loading.display(false);
    });
  }

  delete(id, index){

    console.log(id);
    this.loading.display(true);
    this.apiRequest.postRequest('delete', {id : id}).then( (res) => {

      console.log(res);
      this.loading.display(false);

      swal({position: 'top-end',
        title: "Successfully deleted",
        showConfirmButton: false,
        timer: 1500
      });
      this.users.splice( index, 1);
    })
    .catch( (err)=> {

      this.loading.display(false);
      swal({position: 'top-end',
        title: err['error']['error']['message'],
        showConfirmButton: false,
        timer: 1500
      });
    });
  }

  logout(){

    window.localStorage.clear();
    this.router.navigate(['/auth/login'])
  }
}
