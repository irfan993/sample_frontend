import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, FormControl, Validators } from '@angular/forms';
import { Router } from '@angular/router';
import {AppRequestService} from '../../shared/services/app-request.service';
import {LoadingServiceService} from '../../shared/services/loading-service.service';
import {ToastrService} from 'ngx-toastr';
import {HttpErrorResponse} from '@angular/common/http';
import swal from 'sweetalert2'
import {SocketService} from '../../shared/services/socket.service';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.css']
})
export class LoginComponent implements OnInit {


  loginForm: FormGroup;
  submitted = false;
  constructor(public router: Router, private fb: FormBuilder, private apiRequest : AppRequestService,
              private loading : LoadingServiceService , private toastr: ToastrService, private socketService : SocketService) {

    this.socketService.socket.on('test_Socket' , function (res) {

      console.log(res.test);
    });


    this.loginForm = this.fb.group({

      email: ['',Validators.compose([Validators.required,Validators.pattern('[A-Za-z0-9._%+-]{3,}@[a-zA-Z]{3,}([.]{1}[a-zA-Z]{2,}|[.]{1}[a-zA-Z]{2,}[.]{1}[a-zA-Z]{2,})')])],
      password: ['',Validators.compose([
        Validators.required
      ])]
    });
  }

  ngOnInit() {

/*    var self = this;
    setTimeout( () => {

      console.log('in');
      self.toastr.error("hello");

  },1000)*/

  }

  onSubmit(){

    console.log(this.loginForm.value);
    this.loading.display(true);
    this.apiRequest.postRequest('login', { 'email' : this.loginForm.value.email, password : this.loginForm.value.password}).then( (res) => {


      if(res['success'] ){

        this.loading.display(false);
        this.apiRequest.config.headers['x-access-token'] = res['data']['token'];
        localStorage.setItem('isLoggedin', 'true');
        localStorage.setItem('user_id', res['data']['_id']);
        localStorage.setItem('token', res['data']['token']);
        this.toastr.success('Successfully login');
        this.router.navigate(['users']);
      }else{

        this.loading.display(false);
       // this.toastr.error(res['error']['message']);
        swal({position: 'top-end',
          //type: 'error',
          title: res['error']['message'],
          showConfirmButton: false,
          timer: 1500});
      }
    })
    .catch( (err)=> {

      console.log(err['error']['error']['message']);
      this.loading.display(false);
     // this.toastr.error("h");
    //  this.toastr.error(err['error']['error']['message']);
      swal({position: 'top-end',
       // type: 'error',
        title: err['error']['error']['message'],
        showConfirmButton: false,
        timer: 1500});
    });
  }

}
