import { Component, OnInit } from '@angular/core';
import {FormBuilder, FormGroup, Validators} from '@angular/forms';
import {LoadingServiceService} from '../../shared/services/loading-service.service';
import {ToastrService} from 'ngx-toastr';
import {Router} from '@angular/router';
import {AppRequestService} from '../../shared/services/app-request.service';
import {CommonService} from '../../shared/services/common.service';
import swal from 'sweetalert2'

@Component({
  selector: 'app-register',
  templateUrl: './register.component.html',
  styleUrls: ['./register.component.css']
})
export class RegisterComponent implements OnInit {

  registerForm : FormGroup;
  submitted = false;
  constructor(private fb: FormBuilder, public common : CommonService , public loading : LoadingServiceService,
              public apiRequest : AppRequestService, private toastr: ToastrService, public router: Router) { }

  ngOnInit() {

    this.registerForm = this.fb.group({

      email: ['',Validators.compose([Validators.required])],
      password: ['',Validators.compose([Validators.required])],
      first_name : ['', Validators.compose([Validators.required, Validators.maxLength(50)])],
      last_name : ['', Validators.compose([Validators.required, Validators.maxLength(50)])],
      username : ['', Validators.compose([Validators.required, Validators.maxLength(50)])],
      confirm_password : ['', Validators.compose([Validators.required])],
    }, {validator: this.common.matchingPasswords('password', 'confirm_password')});
  }

  onSubmit(){

    console.log(this.registerForm.value);
    this.loading.display(true);
    this.apiRequest.postRequest('register',  this.registerForm.value).then( (res) => {

      this.loading.display(false);
     // this.toastr.success('Successfully Registered');
      swal({position: 'top-end',
        //type: 'error',
        title: "Successfully Registered",
        showConfirmButton: false,
        timer: 1500
      });
      this.router.navigate(['auth/login']);
    })
    .catch( (err)=> {

      this.loading.display(false);

    //  console.log(err['error']['error']['message']['email']['msg']);
      if(err['error']['error']['message'] && err['error']['error']['message']['email']){

        var t = err['error']['error']['message']['email'];

        swal({position: 'top-end',
          //type: 'error',
          title : t.msg,
          showConfirmButton: false,
          timer: 1500
        });
        return;
      }

     // this.toastr.error(err['error']['message']);
      swal({position: 'top-end',
        //type: 'error',
        title : err['error']['error']['message']['email']['msg'],
        showConfirmButton : false,
        timer : 1500
      });
    });
  }

}
