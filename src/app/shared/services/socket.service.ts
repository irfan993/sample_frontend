import {Inject, Injectable} from '@angular/core';
import * as io from 'socket.io-client';
import * as Constants from "../../config/constants";

@Injectable()
export class SocketService {
  private url;
  public socket;
  constructor() {
    this.url = Constants.AppConst.SOCKET_END_POINT;
    this.socket = io(this.url);
  }
}
