import { Injectable } from '@angular/core';
import * as Constants from "../../config/constants";
import { HttpClient } from '@angular/common/http';



@Injectable({
  providedIn: 'root'
})
export class AppRequestService {

    token = (window.localStorage.getItem('token') == null) ? '' : window.localStorage.getItem('token');
    config = {
        headers : {
            'Content-Type'   : 'application/json',
            'x-access-token' : this.token,
        }
    };

    constructor(public http: HttpClient) {}

    getRequest(pageUrl) {

        return new Promise((resolve,reject) => {
            this.http.get(Constants.AppConst.API_END_POINT+pageUrl,this.config).subscribe(data => {
                resolve(data);
            }, err => {
                reject(err);

            });
        });
    }

    postRequest(pageUrl, data) {

        return new Promise((resolve,reject) => {

            this.http.post(Constants.AppConst.API_END_POINT+pageUrl, data, this.config).subscribe(data => {
                resolve(data);
            }, err => {

                reject(err);

            });
        });
    }
}
