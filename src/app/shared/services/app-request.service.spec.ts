import { TestBed, inject } from '@angular/core/testing';

import { AppRequestService } from './app-request.service';

describe('AppRequestService', () => {
  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [AppRequestService]
    });
  });

  it('should be created', inject([AppRequestService], (service: AppRequestService) => {
    expect(service).toBeTruthy();
  }));
});
