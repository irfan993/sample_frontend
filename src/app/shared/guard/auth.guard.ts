import { Injectable } from '@angular/core';
import { CanActivate } from '@angular/router';
import { Router } from '@angular/router';
import {log} from 'util';

@Injectable()
export class AuthGuard implements CanActivate {

    constructor(private router: Router) {

    }

    canActivate() {

        console.log(localStorage.getItem('isLoggedin'));
        if (localStorage.getItem('isLoggedin')) {

           // this.router.navigate(['users']);
            return true;

        }

        this.router.navigate(['auth/login']);
        return false;
    }
}
